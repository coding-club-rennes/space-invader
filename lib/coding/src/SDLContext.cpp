//
// SDLContext.cpp for minecraft++ in
// /home/thepatriot/thepatriotsrepo/minecraft++
//
// Made by bertho_d
// Login   <bertho_d@epitech.net>
//
// Started on  Sun Aug  3 22:38:29 2014 bertho_d
// Last update Sun Aug  3 23:42:53 2014 bertho_d
//

#include "SDLContext.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "SDLError.hpp"

SDLContext::SDLContext(Uint32 flags) {
  if (SDL_Init(flags) != 0) {
    throw(SDLError("SDL initialization failed"));
  }
  if (TTF_Init() == -1) {
    throw(SDLError("SDL_ttf initialization failed"));
  }
}

SDLContext::~SDLContext() {
  SDL_Quit();
  TTF_Init();
}

bool SDLContext::isInitialized() const { return (true); }
