//
// SDLError.hpp for minecraft++ in /home/thepatriot/thepatriotsrepo/minecraft++
// 
// Made by bertho_d
// Login   <bertho_d@epitech.net>
// 
// Started on  Sun Aug  3 19:14:35 2014 bertho_d
// Last update Mon Aug  4 01:19:14 2014 bertho_d
//

#ifndef SDLERROR_HPP_
# define SDLERROR_HPP_

# include "Error.hpp"

class		SDLError : public Error
{
public:
  SDLError();
  SDLError(const char *error);
};

#endif
